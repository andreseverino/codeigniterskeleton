<?php
    require_once("fpdf.php");

    class Pdf extends FPDF 
    {
        
        protected $textoRodape1  = '';
        protected $textoRodape2  = '';
        protected $textoTopo1    = '';
        protected $textoTopo2    = '';
        
        protected $img_topo         = '';
        protected $img_bkg         = '';
        protected $img_rodape       = '';
        protected $img_logomarca    = '';
        
        function setTopoImagem($img_url)
        {
            $this->img_topo = $img_url;
        }
        
        function setBkgImagem($img_url)
        {
            $this->img_bkg = $img_url;
        }
        
        function setRodapeImagem($img_url)
        {
            $this->img_rodape = $img_url;
        }
        
        function setLogomarcaImagem($img_url)
        {
            $this->img_logomarca = $img_url;
        }
        
        public function setRodapeTexto1( $texto )
        {
            $this->textoRodape1 = $texto;
        }
        
        public function setRodapeTexto2( $texto )
        {
            $this->textoRodape2 = $texto;
        }
        
        public function setTopoTexto1( $texto )
        {
            $this->textoTopo1 = $texto;
        }
        
        public function setTopoTexto2( $texto )
        {
            $this->textoTopo2 = $texto;
        }
        
        // CABEÇALHO
        public function Header()
        {
            if( $this->img_bkg != '')
            {
                $this->Image( $this->img_bkg, 10, 65, 190, '', 'JPG', '' );
            }
            if( $this->img_topo != '' )
            {
                $this->Image( $this->img_topo, 0, 0, 210, 25, 'JPG', '' );
            }
            else
            {
                $this->SetTextColor(0,0,0);
                $this->SetLineWidth(0.2);
                $this->SetDrawColor(143,143,143);
                
                if( $this->img_logomarca != '' )
                {
                    $this->Image( $this->img_logomarca, 0, 0, 50, 25, 'JPG', '' );
                }
                if( $this->textoTopo1 != '' )
                {
                    $this->SetY(4);
                    $this->SetX(60);
                    $this->SetFont('Arial','B','14');
                    $this->Cell(190,5, utf8_decode($this->textoTopo1),0,0,'L');
                }
                if( $this->textoTopo2 != '' )
                {
                    $this->Ln(5);
                    $this->SetY(12);
                    $this->SetX(60);
                    $this->SetFont('Arial','','08');
                    $this->Cell(190,5, utf8_decode($this->textoTopo2),0,0,'L');
                }
                
                $this->Line(0, 25, 220, 25);
            }
            
            $this->SetY(30);
        }
        
        function Footer()
        {
            if( $this->img_rodape != '' )
            {
                $this->SetY(-15);
                $this->Image( $this->img_rodape, 0, 282, 210, 15, 'JPG', '' );
            }
            else
            {
                $this->SetY(-15);
                $this->SetTextColor(0,0,0);
                $this->SetLineWidth(0.2);
                $this->SetDrawColor(143,143,143);
                $this->SetFont('Arial','B','08');
                if( $this->textoRodape1 != '' )
                {
                    $this->Cell(190,5,utf8_decode($this->textoRodape1),0,0,'C');
                }
                if( $this->textoRodape2 != '' )
                {
                    $this->Line(10, 287, 200, 287);
                    $this->Ln(5);
                    $this->SetFont('Arial','','08');
                    $this->Cell(190,5,utf8_decode($this->textoRodape2),0,0,'C');
                    $this->SetX(160);
                }
            }
        }

        function NbLines($w, $txt)
        {
            $cw=&$this->CurrentFont['cw'];
            if($w==0)
                $w=$this->w-$this->rMargin-$this->x;
            $wmax=($w-2*$this->cMargin)*1000/$this->FontSize;
            $s=str_replace("\r", '', $txt);
            $nb=strlen($s);
            if($nb>0 and $s[$nb-1]=="\n")
                $nb--;
            $sep=-1;
            $i=0;
            $j=0;
            $l=0;
            $nl=1;
            while($i<$nb)
            {
                $c=$s[$i];
                if($c=="\n")
                {
                    $i++;
                    $sep=-1;
                    $j=$i;
                    $l=0;
                    $nl++;
                    continue;
                }
                if($c==' ')
                    $sep=$i;
                $l+=$cw[$c];
                if($l>$wmax)
                {
                    if($sep==-1)
                    {
                        if($i==$j)
                            $i++;
                    }
                    else
                        $i=$sep+1;
                    $sep=-1;
                    $j=$i;
                    $l=0;
                    $nl++;
                }
                else
                    $i++;
            }
            return $nl;
        }
    }
?>
