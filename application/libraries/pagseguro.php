<?php

class Pagseguro
{

    private $_link  = 'https://ws.pagseguro.uol.com.br/v2/checkout/';
    private $_email;
    private $_token;
    private $_referencia;
    private $_moeda = 'BRL';
    private $_itens=array();
    private $_cliente_nome;
    private $_cliente_ddd;
    private $_cliente_telefone;
    private $_cliente_email;
    private $_errors;

    function __construct(){ }

    private function validaEmail( $email )
    {
        $return = FALSE;
        if( !empty( $email ) )
        {
            $pattern = '/^[a-zA-Z0-9\._-]+@[a-zA-Z0-9\._-]+.([a-zA-Z]{2,4})$/';

            if( preg_match( $pattern, $email ) )
                $return = $email;
            else
                $return = FALSE;
        }

        return $return;
    }

    private function getFields()
    {
        $data['token']              = $this->_token;
        $data['currency']           = $this->_moeda;
        $data['itemId1']            = $this->_itens[0]['itemId1'];
        $data['itemDescription1']   = $this->_itens[0]['itemDescription1'];
        $data['itemAmount1']        = number_format( $this->_itens[0]['itemAmount1'], 2, '.', '');
        $data['itemQuantity1']      = $this->_itens[0]['itemQuantity1'];
        $data['itemWeight1']        = $this->_itens[0]['itemWeight1'];

        $return = 'email='.$this->_email.'&token='.$this->_token.'&currency='.$this->_moeda.'&reference='.$this->_referencia.'&itemId1='.$data['itemId1'].'&itemDescription1='.$data['itemDescription1'].'&itemAmount1='.$data['itemAmount1'].'&itemQuantity1='.$data['itemQuantity1'].'&itemWeight1='.$data['itemWeight1'];

        #$return = 'email=andre@andrewd.com.br&token=3F849EE06E564A4682D4A93C4807BB00&currency=BRL&itemId1=1&itemDescription1=curso&itemAmount1=12.12&itemQuantity1=1&itemWeight1=0';

        return $return;
    }

    function setEmail( $email )
    {
        if( !empty( $email ) && $this->validaEmail( $email ) )
            $this->_email = $email;
    }

    function setToken( $token )
    {
        if( !empty($token) )
            $this->_token = $token;
    }

    function setReferencia( $referencia )
    {
        if( !empty($referencia) )
            $this->_referencia = $referencia;
    }

    function setMoeda( $moeda )
    {
        if( !empty( $moeda ) )
            $this->_moeda = $moeda;
    }

    function setItem( $item )
    {
        if( is_array($item) )
        {
            $this->_itens[] = $item;
        }
    }

    function setClienteNome( $nome )
    {
        if( !empty( $nome ) )
            $this->_cliente_nome = $nome;
    }

    function setClienteEmail( $email )
    {
        if( !empty( $email ) && $this->validaEmail($email) )
            $this->_cliente_email = $email;
    }

    function setClienteDDD( $ddd )
    {
        if( !empty( $ddd ) )
            $this->_cliente_ddd = $ddd;
    }

    function setClienteTelefone( $telefone )
    {
        if( !empty( $telefone ) )
            $this->_cliente_telefone = $telefone;
    }

    private function setErrors( $error )
    {
        if( !empty( $error ) )
            $this->_errors = $error;
    }

    function getErrors()
    {
        return $this->_errors;
    }
    
    function enviaDados()
    {
        $ch = curl_init();

        curl_setopt( $ch, CURLOPT_SSL_VERIFYPEER, false );
        curl_setopt( $ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1 );
        curl_setopt( $ch, CURLOPT_URL, $this->_link );
        curl_setopt( $ch, CURLOPT_POST, 1 );
        curl_setopt( $ch, CURLOPT_POSTFIELDS, $this->getFields() );
        curl_setopt( $ch, CURLOPT_RETURNTRANSFER, 1 );

        $saida = curl_exec( $ch );

        if( curl_errno($ch) )
            $this->_errors = curl_error($ch);
        else
            $this->_errors = NULL;

        curl_close($ch);

        return $saida;
    }






    
}











