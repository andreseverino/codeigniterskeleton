<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


    class Jpgraph {
        
        protected $titulo_x = "Valor (R$)";
        
        protected $cores = array(
                                '#FF3D3D', '#6495ED', '#44e4c8', '#167b09', '#f56eda', 
                                '#8f750b', '#ff6a18', '#000000', '#ab0aff', '#2f384d',
                                '#41fdfb', '#0c1283', '#6cf356', '#0a6d8d', '#3f2e04',
                                '#7e7e7e', '#22430c', '#3f2e04', '#BBBBB0', '#FF9900',
                                '#4d648d', '#408b7e', '#43ac35', '#76386a', '#804949',
                                '#a59b74', '#eca076', '#858282', '#b5a3be', '#73809e',
                                '#448382', '#444559', '#9bd093', '#00c1ff', '#fcce5c',
                                '#ccced3', '#797979', '#f3d281', '#caca97', '#e88e08',
                                '#cccccc', '#cccccc', '#cccccc', '#cccccc', '#cccccc',
                                '#cccccc', '#cccccc', '#cccccc', '#cccccc', '#cccccc',
                                '#cccccc', '#cccccc', '#cccccc', '#cccccc', '#cccccc',
                                '#cccccc', '#cccccc', '#cccccc', '#cccccc', '#cccccc',
                                '#cccccc', '#cccccc', '#cccccc', '#cccccc', '#cccccc',
                                '#cccccc', '#cccccc', '#cccccc', '#cccccc', '#cccccc',
                                '#cccccc', '#cccccc', '#cccccc', '#cccccc', '#cccccc',
                                '#cccccc', '#cccccc', '#cccccc', '#cccccc', '#cccccc',
                                '#cccccc', '#cccccc', '#cccccc', '#cccccc', '#cccccc',
                                '#cccccc', '#cccccc', '#cccccc', '#cccccc', '#cccccc',
                                '#cccccc', '#cccccc', '#cccccc', '#cccccc', '#cccccc'
                            );
        
        function controle_de_metas( $posicao = array(), $titulo, $dados, $legenda, $label )
        {
            require_once("src/jpgraph.php");
            require_once("src/jpgraph_line.php"); 
            
            $graph = new Graph( $posicao['largura'], $posicao['altura'] );
            $graph->SetScale( "textlin" );
            
            $theme_class = new UniversalTheme;

            $graph->SetTheme( $theme_class );
            $graph->img->SetAntiAliasing();
            $graph->img->SetMargin(70,50,50,70);
            $graph->SetBox(false);
            $graph->title->Set( $titulo );

            $graph->yaxis->title->Set("Valor (R$)");
            $graph->yaxis->title->SetMargin(28);
            $graph->yaxis->title->SetFont(FF_ARIAL,FS_BOLD,11);
            //$graph->xaxis->title->Set("Dias");
            $graph->xaxis->title->SetMargin(5);
            $graph->xaxis->title->SetFont(FF_ARIAL,FS_BOLD,11);

            $graph->yaxis->HideZeroLabel();
            $graph->yaxis->HideLine(false);
            $graph->yaxis->HideTicks(false,false);

            $graph->xgrid->Show();
            $graph->xgrid->SetLineStyle("solid");
            $graph->xgrid->SetColor('#E3E3E3');
            $graph->xaxis->SetTickLabels( $label['nome'] );
            
            for($i=0; $i<count($legenda['id']); $i++)
            {
                $usu_id = $legenda['id'][$i];
                
                $dataLinePlot = array(
                                (int)$dados[$usu_id]['A'], 
                                (int)$dados[$usu_id]['P'], 
                                (int)$dados[$usu_id]['G']
                            );
                
                $p1 = new LinePlot( $dataLinePlot );
                $graph->Add( $p1 );
                $p1->SetColor( $this->cores[$i] );
                $p1->SetLegend( $legenda['nome'][$i] );
            }

            $graph->legend->SetFrameWeight(1);

            return $graph; 
        }
        
        function analise_resultados_vendedores( $posicao = array(), $titulo, $dados, $legenda, $label, $type )
        {
            require_once("src/jpgraph.php");
            require_once("src/jpgraph_line.php"); 
            
            $graph = new Graph( $posicao['largura'], $posicao['altura'] );
            $graph->SetScale( "textint" );
            
            $theme_class = new UniversalTheme;

            $graph->SetTheme( $theme_class );
            $graph->img->SetAntiAliasing(true);
            $graph->img->SetMargin(70,40,40,70);
            $graph->SetBox(false);
            $graph->title->Set( $titulo );

            $graph->yaxis->HideZeroLabel();
            $graph->yaxis->HideLine(false);
            $graph->yaxis->HideTicks(false,false);

            $graph->xgrid->Show();
            $graph->xgrid->SetLineStyle("solid");
            $graph->xgrid->SetColor('#E3E3E3');
            $graph->xaxis->SetTickLabels( $label['nome'] );

            
            $graph->yaxis->title->Set("Valor (R$)");
            $graph->yaxis->title->SetMargin(28);
            $graph->yaxis->title->SetFont(FF_ARIAL,FS_BOLD,11);
            $graph->xaxis->title->Set("Meses");
            $graph->xaxis->title->SetMargin(5);
            $graph->xaxis->title->SetFont(FF_ARIAL,FS_BOLD,11);

            $type = $type == 'A' ? 12 : 31;
            
            for($i=0; $i<count($legenda['id']); $i++)
            {
                $id_usuario     = $legenda['id'][$i];
                $nome_usuario   = $legenda['nome'][$i];
                
                $dataLinePlot = array();
                for( $v=1; $v<=$type; $v++ )
                {
                    $valor = $dados[$id_usuario][$v]['valor'];
                    $dataLinePlot[] = $valor;
                }
                
                $p1 = new LinePlot( $dataLinePlot );
                $graph->Add( $p1 );
                $p1->SetColor( $this->cores[$i] );
                $p1->SetLegend( $nome_usuario );
            }

            return $graph; 
        }


        function analise_resultados_vendedores_data( $posicao = array(), $titulo, $dados, $legenda, $label, $type )
        {
            require_once("src/jpgraph.php");
            require_once("src/jpgraph_line.php");

            $graph = new Graph( $posicao['largura'], $posicao['altura'] );
            $graph->SetScale( "textint" );

            $theme_class = new UniversalTheme;

            $graph->SetTheme( $theme_class );
            $graph->img->SetAntiAliasing(true);
            $graph->img->SetMargin(70,50,50,70);
            $graph->SetBox(false);
            $graph->title->Set( $titulo );

            $graph->yaxis->HideZeroLabel();
            $graph->yaxis->HideLine(false);
            $graph->yaxis->HideTicks(false,false);

            $graph->yaxis->title->Set($this->titulo_x);
            $graph->yaxis->title->SetMargin(28);
            $graph->yaxis->title->SetFont(FF_ARIAL,FS_BOLD,11);
            $graph->xaxis->title->Set("Dias");
            $graph->xaxis->title->SetMargin(5);
            $graph->xaxis->title->SetFont(FF_ARIAL,FS_BOLD,11);


            $graph->xgrid->Show();
            $graph->xgrid->SetLineStyle("solid");
            $graph->xgrid->SetColor('#E3E3E3');
            $graph->xaxis->SetTickLabels( $label['nome'] );

            for($i=0; $i<count($legenda['id']); $i++)
            {
                $id_usuario     = $legenda['id'][$i];
                $nome_usuario   = $legenda['nome'][$i];

                $dataLinePlot = array();
                for( $v=0; $v<count($label['id']); $v++ )
                {
                    $valor = $dados[$id_usuario][$label['id'][$v]]['valor'];
                    $dataLinePlot[] = $valor;
                }

                $p1 = new LinePlot( $dataLinePlot );
                $graph->Add( $p1 );
                $p1->SetColor( $this->cores[$i] );
                $p1->SetLegend( $nome_usuario );
            }

            return $graph;
        }

        function setTituloX($titulo){
            $this->titulo_x = $titulo;
        }
        
    } 
    
    
    