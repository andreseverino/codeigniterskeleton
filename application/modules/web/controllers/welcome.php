<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Welcome extends MY_Controller {
	
	public function __construct()
	{
		$this->_modulo      = MODULO_WEB;
        $this->_controller  = strtolower( __CLASS__ );
		parent::__construct();
	}
	
	public function index()
	{
		$this->load->view( $this->_modulo . $this->_controller . '/index' );
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */