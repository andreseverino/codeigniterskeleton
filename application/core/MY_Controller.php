<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

    class MY_Controller extends MX_Controller 
	{
        protected $_module;
        protected $_controller;
       
        public function  __construct()
		{ 
			parent::__construct();
		}
        
        protected function _mensagem_status( $status, $msg, $url )
        {
            $array = array( 'status' => 'status_' . $status, 
							'msg' 	 => $msg );
            $this->session->set_flashdata( 'msg_status', $array );
            redirect( $url );
        }
    }

    /* End of file MY_Controller.php */
    /* Location: ./app/core/MY_Controller.php */
