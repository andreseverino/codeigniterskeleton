/*
Copyright (c) 2003-2011, CKSource - Frederico Knabben. All rights reserved.
For licensing, see LICENSE.html or http://ckeditor.com/license
*/
CKEDITOR.editorConfig = function( config )
{
	config.language = 'pt-br';
        config.toolbar_Basica_1 = 
        [
            //{ name: 'clipboard', items : [ 'Paste','PasteText' ] },
            { name: 'basicstyles', items : [ 'Bold','Italic','Underline' ] },
            { name: 'paragraph', items : [ 'NumberedList','BulletedList' ] },
            { name: 'links', items : [ 'Link','Unlink' ] }
        ];
        config.toolbar_Basica_2 = 
        [
            //{ name: 'clipboard', items : [ 'Paste','PasteText' ] },
            { name: 'basicstyles', items : [ 'Bold','Italic','Underline' ] },
            { name: 'paragraph', items : [ 'NumberedList','BulletedList' ] },
            { name: 'links', items : [ 'Link','Unlink' ] },
            { name: 'image', items : [ 'Image' ] },
            { name: 'insert', items : [ 'Table' ] }
        ];
        config.toolbar_Basica_3 = 
        [
            //{ name: 'clipboard', items : [ 'Paste','PasteText' ] },
            //{ name: 'styles', items : [ 'Styles','Format','Font','FontSize' ] },
            { name: 'styles', items : [ 'Styles','Format' ] },
            { name: 'basicstyles', items : [ 'Bold','Italic','Underline' ] },
            { name: 'paragraph', items : [ 'NumberedList','BulletedList' ] },
            { name: 'links', items : [ 'Link','Unlink' ] },
            { name: 'insert', items : [ 'Table' ] }
        ];
};